module.exports = {
    sections: {
        LoginPageHRA : {
            selector: 'body',
            elements: {
                username: {locateStrategy:'xpath',selector: "//input[@name='login_id']"},
                password: {locateStrategy:'xpath',selector: "//input[@name='password']"},
                LoginButton: {locateStrategy:'xpath',selector: "//input[@name='doLogin']"},
                incorrectPassword: {locateStrategy:'xpath',selector: "//div[@class='hralogin_div']"},
                Vulnerable: {locateStrategy:'xpath',selector: "//p[contains(text(),'This website is vulnerable to Clickjacking')]"},
                ForgotPassword: {locateStrategy:'xpath',selector: "//p[@id='forgotpassword']"},
                RequestPassword: {locateStrategy:'xpath',selector: "//input[@name='forgot_password']"},
                ServerError: {locateStrategy:'xpath',selector: "//h1[contains(text(),'Server Error')]"}


                }

            },
        DashboardPage: {
            selector: 'body',
            elements: {
                Welcome: {locateStrategy:'xpath',selector: "//h1[contains(text(),'Welcome, zWdnkog')]"},
                Claims: {locateStrategy:'xpath',selector: "//ul[@class='dropdown menu']/li[2]"},
                OneTimeClaim: {locateStrategy:'xpath',selector: "//div[@class='hrafloatright']/a[2]"},
                Claimant: {locateStrategy:'xpath',selector: "//select[@name='pat_relation_emp']"},
                ClaimantName: {locateStrategy:'xpath',selector: "//select[@name='pat_relation_emp']/option[2]"},
                ServiceDate: {locateStrategy:'xpath',selector: "//input[@name='pat_date_ser']"},
                ClaimAmount: {locateStrategy:'xpath',selector: "//input[@name='pat_claim_amt']"},
                ExpenseIncured: {locateStrategy:'xpath',selector: "//select[@name='pat_serv_perf']"},
                HealthPremium: {locateStrategy:'xpath',selector: "//select[@name='pat_serv_perf']/option[1]"},
                DOE: {locateStrategy:'xpath',selector: "//input[@name='pat_reason_4v']"},
                ProviderName:  {locateStrategy:'xpath',selector: "//input[@name='pat_prov_name']"},
                ChooseFile: {locateStrategy:'xpath',selector: "//input[@name='attached_doc']"},
               // CB1: {locateStrategy:'xpath',selector: "//input[@name='auth_submit']"},
               // CB2: {locateStrategy:'xpath',selector: "//input[@name='auth_fraud']"},
               // Submit: {locateStrategy:'xpath',selector: "//input[@name='review_submit']"},
                Error: {locateStrategy:'xpath',selector: "//text[contains(text(),'invalid file type.')]/../text[contains(.,'Accepted types include: .jpg  .jpeg  .png  .bmp .tiff  .gif  .pdf  .doc  .docx  .xps')]"},
                AddtoList: {locateStrategy:'xpath',selector: "//input[@name='upload_document']"},
                ErrorEncountered: {locateStrategy:'xpath',selector: "//h1[contains(text(),'An Error Was Encountered')]"}

            }

        },



            }};