var data = require('../../TestResources/HRAData');
var Objects = require(__dirname + '/../../repository/HRA_XeosPages.js');
//var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');

var LoginPageHRA,DashboardPage;
function initializePageObjects(browser, callback) {
    var BPPage = browser.page.HRA_XeosPages();
    LoginPageHRA = BPPage.section.LoginPageHRA;
    DashboardPage= BPPage.section.DashboardPage;
    callback();
}
//1
module.exports = function() {
    this.Given(/^User opens the SSO Url$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
                if (execEnv.toUpperCase() == "QA") {
            URL = data.HRAXeosURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);

            });
        }
    });







    //2

    this.Given(/^User opens the browser and launch the HRAXeos Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
         console.log('Test Environment: ' + execEnv);
         if (execEnv.toUpperCase() == "QA") {
             URL = data.HRAXeosURL1;
             initializePageObjects(browser, function () {
                 browser.maximizeWindow()
                     .deleteCookies()
                     .url(URL);
                 browser.timeoutsImplicitWait(30000);
                 LoginPageHRA.waitForElementVisible('@username', data.wait);
             });
         }
    });
    this.When(/^User Login to the HRA XEOS application using valid Internal User Login credentials$/, function () {
        var URL;
        browser = this;
       var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@username', data.wait)
                .waitForElementVisible('@password', data.wait)
                .setValue('@username',data.Username)


                .setValue('@password',data.Password);



        }
    });
    this.Then(/^User data are transported in encrypted format$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.click('@LoginButton');

        }
    });


    //3
    this.When(/^try to login with incorrect username or password$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.setValue('@username',data.username1)
                .setValue('@password',data.password1)
            .click('@LoginButton');
        }
    });

    this.Then(/^User enters valid credentials$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.setValue('@username',data.Username)
                .setValue('@password',data.Password)
            .click('@LoginButton');
        }
    });

    //4

    this.When(/^User enters a different url$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.HRAXeosURL2;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPageHRA.waitForElementVisible('@username', data.wait);
            });
        }
    });


    //5

    this.When(/^User enters incorrect login credentials$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA
                .setValue('@username',data.incusername)
                .setValue('@password',data.incpassword)
                .click('@LoginButton');
        }
    });




    this.Then(/^User must see message Either Username or Password incorrect$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@incorrectPassword',data.wait);
        }
    });

    //6
   this.Given(/^User open the html file in any browser$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            URL=data.HRAXeosHTML;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);

            });
        }
    });


    this.When(/^check whether the site is vulnerale or not$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA" ) {
            var vulnerableText= "//p[contains(text(),'This website is vulnerable to Clickjacking')]";

               browser.pause(5000)
            browser.useXpath().waitForElementPresent(vulnerableText, data.wait);



        }
    });

     //7
    this.When(/^User changes https to http$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.HRAXeosURL3;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                          });
        }
    });

    this.Then(/^User must be redirected to actual URL$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@username',data.wait)
                .waitForElementVisible('@password',data.wait);
        }
    });


//8
    this.When(/^User logs in with valid credentials and does not use for specified time limit$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.setValue('@username',data.Username)
                .setValue('@password',data.Password)
                .click('@LoginButton');
            browser.pause(1200000)
            .refresh();
        }
    });

    this.When(/^Application has automatically logged you out$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@username', data.longwait)
                .waitForElementVisible('@password', data.longwait);
        }
    });

    //9
    this.When(/^User clicks on username field$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@username', data.wait)
                .click('@username');
            browser.pause(10000);

        }
    });

    this.Then(/^browser must not prompt with recently used usernames$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@username', data.wait)
                        }
    });

    //13
    this.When(/^User Login to the HRA XEOS application using valid credentials$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@username', data.wait)
                .waitForElementVisible('@password', data.wait)
                .setValue('@username',data.Username)


                .setValue('@password',data.Password)
                .click('@LoginButton');



        }
    });

    this.Then(/^User navigates to claim page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            DashboardPage.waitForElementVisible('@Claims', data.wait)
                .click('@Claims');
        }
    });

    this.Then(/^User clicks on Create ont time claim$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            DashboardPage.waitForElementVisible('@OneTimeClaim', data.wait)
                .click('@OneTimeClaim');

        }
    });


    this.Then(/^User enters all the details and clicks on the upload button$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            DashboardPage.waitForElementVisible('@Claimant', data.wait)
                .click('@Claimant')
            .waitForElementVisible('@ClaimantName', data.wait)
                .click('@ClaimantName')
        .waitForElementVisible('@ServiceDate', data.wait)
                .setValue('@ServiceDate',data.servicedate)
        .waitForElementVisible('@ClaimAmount', data.wait)
                .clearValue('@ClaimAmount')
                .setValue('@ClaimAmount',data.amt)
         .waitForElementVisible('@ExpenseIncured', data.wait)
                .click('@ExpenseIncured')
        .waitForElementVisible('@HealthPremium', data.wait)
                .click('@HealthPremium')
        .waitForElementVisible('@DOE', data.wait)
                .setValue('@DOE',data.doe)
                .waitForElementVisible('@ProviderName', data.wait)
                .setValue('@ProviderName',data.prov)
        .waitForElementVisible('@ChooseFile', data.wait)
              browser.setValue('input[type="file"]',require('path').resolve('C:\\Users\\Hemant-Sagar\\Desktop\\setup.exe'))
            DashboardPage.waitForElementVisible('@AddtoList', data.wait)
                .click('@AddtoList');
        }
    });



    this.Then(/^User gets a error message after uploading a .exe file$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            DashboardPage.waitForElementVisible('@Error', data.wait)
                .assert.containsText('@Error','invalid file type.','Accepted types include: .jpg  .jpeg  .png  .bmp .tiff  .gif  .pdf  .doc  .docx  .xps');

        }
    });



    this.When(/^User enters sql url$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.SQLUrl;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
            });
        }
    });

    this.Then(/^User must see error message$/, function () {
                browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            DashboardPage.waitForElementVisible('@ErrorEncountered', data.wait)
                .assert.containsText('@ErrorEncountered','An Error Was Encountered');

        }
    });

    //11
    this.When(/^User enters incorrect login credential$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA
                .setValue('@username',data.incusername)
                .setValue('@password',data.incpassword);

        }
    });


    this.Then(/^User clicks on forgot password$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@ForgotPassword', data.wait)
                .click('@ForgotPassword');

        }
    });

    this.Then(/^User requests for a new password$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@RequestPassword', data.wait)
                .click('@RequestPassword');

        }
    });

    //10
    this.Then(/^User must recieve error message$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA" ) {
            LoginPageHRA.waitForElementVisible('@ServerError', data.wait);


        }
    });
}